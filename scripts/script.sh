#!/bin/bash

echo Hopefully, build.sh has been run before this script can take effect.
echo If not, the CI has messed up.

for d in */
do
    ( safed=${d%"."} && number=$RANDOM && cd "$d" && java -jar ../build.jar --version "$number" --input . --output ../upload --manifest-dest "../upload/$safed.json" )
done
