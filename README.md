# Nahimic Launcher Modpacks [![Build Status](https://travis-ci.com/Tomoli75/Nahimic-Launcher-Self-Edit-Modpack.svg?branch=master)](https://travis-ci.com/Tomoli75/Nahimic-Launcher-Self-Edit-Modpack)

# About the gh-pages branch
This is a branch created by Travis-Ci. We do not use github pages to host the files, they are cloned from the branch to a web server.

# This is self-serve!
By utilising the CodeOwners function, anyone trying to edit your modpack will require your permission, and ours.

## Warning for pull requesters
Your code will be flagged for review from the modpack owner no matter what.
Uploading .jar files will require a manual review from our official team, to stop DMCA requests.
Please, please create a .url.txt file when uploading a mod. This should contain a direct link to the mod's file.
### IF YOU ARE WORKING ON A PULL REQUEST, PLEASE PUT "WIP" IN THE TITLE!

## For Mod Developers
### We require that any mods uploaded here also download from your site, as to prevent people from just downloading your mods from us. Any copies stored in this repository are accompanied with a .url.txt file, meaning our launcher downloads from your site. The files here are just to make sure no corruption occurs.
### ![StopModReposts](http://i.imgur.com/J4rrGt6.png)

## You may have to wait some time between changes here and the launcher updating, as continuous integration must pass your files before

# Current features
- [ ] Auto-publish from command-line
- [x] Kind of complete ^^
- [x] Support StopModReposts.org
- [ ] Require specific groups of people to check the mods uploaded are all downloaded safely
- [x] Be cool
